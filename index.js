// console.log('qwe');
const showTitle = [];

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json()).then(array => {
   array.map((list) => {
       showTitle.push(list.title);
  })
})

console.log(showTitle);


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method  : 'GET',
   headers : {
      'Content-Type' : 'application/json'
   }
})
.then(response => response.json())
.then(json => console.log(`The item ${json.title} on the list has a status of ${json.completed}`));

fetch('https://jsonplaceholder.typicode.com/todos', {
   method  : 'POST',
   headers : {
      'Content-Type' : 'application/json'
   },
   body: JSON.stringify({
      completed : false,
      title     : 'Create To Do List Item',
      userID    : 1
   })
})
.then(response => response.json())
.then(json => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method  : 'PUT',
   headers : {
      'Content-Type' : 'application/json'
   },
   body: JSON.stringify({
      dateCompleted : 'Pending',
      description   : 'update to do list',
      status        : 'Pending',
      title         : 'Updated To Do List Item',
      userID        : 1
   })
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method : 'PATCH',
   headers : {
      'Content-Type' : 'application/json'
   },
   body : JSON.stringify({
      dateCompleted : "07/11/21",
      status        : "Complete"
   })
})
.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method : 'DELETE'
}) 